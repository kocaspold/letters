class RoomsController < ApplicationController
  def index
    redirect_to room_path(generate_room_name)
  end

  def show

  end

  private

  def generate_room_name
    (0...6).map { (65 + rand(26)).chr }.join
  end
end
