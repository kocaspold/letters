# this file plays role of Controller in Action Cable JS
$(".rooms.show").ready ->
  if $(".rooms.show").length > 0
    # looks for room name saved in frontend
    # TODO: change to take from url instead
    room_name = $("#room").attr("name")

    # creates subscription with a name and additional param of room_name
    App.game = App.cable.subscriptions.create { channel: "GameChannel", room_name: room_name},
      connected: ->
        @printMessage("Waiting for opponent...")

      received: (data) ->
        switch data.action
          when "game_start"
            @printMessage("Game started!")
            App.hideFriendLink()
          when "opponent_forfeits"
            App.showYouWin("Opponent left the game.")
          when "add_letter"
            if data.msg.side is "right"
              $(".game_word").each ->
                $(this).append(data.msg.letter.toUpperCase())
            if data.msg.side is "left"
              $(".game_word").each ->
                $(this).prepend(data.msg.letter.toUpperCase())
            @printMessage("Letter added: " + data.msg.letter + " " + data.msg.side)
          when "set_turn"
            App.showControles()
            PageTitleNotification.On("Your turn!");

            App.favicon=new Favico({
                animation:'slide'
            });
            App.favicon.badge('!')

          when "won"
            App.showYouWin(data.msg)
            PageTitleNotification.Off();
            App.favicon.badge('')
          when "lost"
            App.showYouLoose(data.msg)
            PageTitleNotification.Off();
            App.favicon.badge('')
          when "check_word"
            App.askForWord(data.msg.message)

      printMessage: (message) ->
        $("#messages").prepend("<p>#{message}</p>")
